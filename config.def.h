static const char *background_color = "#002b36";
static const char *border_color = "#000000";
static const char *font_color = "#839496";
static const char *font_pattern = "terminus:pixelsize=14:antialias=true:autohint=true";
static const unsigned line_spacing = 5;
static const unsigned int padding = 7;

static const unsigned int width = 250;
static const unsigned int border_size = 1;
static const unsigned int pos_x = 0;
static const unsigned int pos_y = 0;

enum corners { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
enum corners corner = TOP_RIGHT;

static const unsigned int duration = 2; /* in seconds */

#define DISMISS_BUTTON Button1
#define ACTION_BUTTON Button3
